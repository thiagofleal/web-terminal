import express from "express";
import { join } from "path";

import { rootdir } from "./rootdir"

const app = express();
const port = 8080;

app.use("/", express.static(join(rootdir, "..", "frontend")));

app.listen(port, () => {
  console.log(`Server started at 0.0.0.0:${ port }`);
});
